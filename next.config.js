module.exports = {
  reactStrictMode: true,
  async rewrites() {
    return [{ source: "/", destination: "/countries" }];
  },
  images: {
    domains: ["media.api-sports.io"],
  },
};
