import { Box, Button, Select } from "@chakra-ui/react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { League, Season } from "types";
import styles from "./country-card.module.css";

interface CountryCardProps {
  league: League;
  seasons: Season[];
}

export const CountryCard: React.FC<CountryCardProps> = ({
  league,
  seasons,
}) => {
  const router = useRouter();
  
  const [selectedYearsByLeagueId, setSelectedYears] = React.useState<{
    [leagueId: string]: string;
  }>({});

  const handleYearChange = (
    event: React.ChangeEvent<HTMLSelectElement>,
    leagueId: number
  ) => {
    const value = event.target.value;

    setSelectedYears((prevState) => ({ ...prevState, [leagueId]: value }));
  };

  return (
    <Box borderWidth="1px" borderRadius="lg" className={styles.wrapper}>
      <div>
        <div className={styles.description}>
          {league.logo && (
            <Image
              width={100}
              height={100}
              alt={league.name}
              src={league.logo}
            />
          )}
          <span className={styles.leagueName}>{league.name}</span>
        </div>
        <span>
          Playing format: <b>{league.type}</b>
        </span>
      </div>
      {seasons.length > 0 && (
        <Select
          defaultValue={""}
          placeholder="year"
          onChange={(event) => handleYearChange(event, league.id)}
        >
          {seasons.map((season) => (
            <option key={season.start}>{season.year}</option>
          ))}
        </Select>
      )}
      {selectedYearsByLeagueId[league.id] && (
        <Link
          href={`/countries/${router.query.country}/leagues/${
            league.id
          }?season=${selectedYearsByLeagueId[league.id]}`}
        >
          <a>
            <Button
              colorScheme="teal"
              variant="outline"
              className={styles.goForwardButton}
            >
              Go forward
            </Button>
          </a>
        </Link>
      )}
    </Box>
  );
};
