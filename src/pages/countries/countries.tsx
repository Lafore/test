import { NextPage, GetServerSideProps } from "next";
import React from "react";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import axios from "axios";
import { List, Input } from "@chakra-ui/react";
import { Country } from "types";
import { API } from "api";
import styles from "./countries.module.css";

interface CountriesProps {
  data: Country[];
}

const Countries: NextPage<CountriesProps> = ({ data = [] }) => {
  const [searchValue, setSearchValue] = React.useState("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;

    setSearchValue(value.toLowerCase());
  };

  const filteredCountries = data.filter(
    ({ name, code }) =>
      (name && name.toLowerCase().startsWith(searchValue)) ||
      (code && code.toLowerCase().startsWith(searchValue))
  );

  return (
    <div>
      <Head>
        <title>Countries</title>
        <meta
          name="description"
          content="Football content - Football countries"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <h1>Country you can choose:</h1>
        <Input
          placeholder="Search the specific country..."
          className={styles.search}
          onChange={handleChange}
        />
        {filteredCountries.length > 0 && (
          <List className={styles.list}>
            {filteredCountries.map((country) => (
              <Link
                key={country.name}
                href={`/countries/${country.name.toLowerCase()}`}
              >
                <a className={styles.itemBox}>
                  <div className={styles.item}>
                    {country.flag && (
                      <Image
                        width={50}
                        height={50}
                        alt={country.name}
                        src={country.flag}
                        className={styles.flag}
                      />
                    )}
                    <span className={styles.countryName}>{country.name}</span>
                  </div>
                </a>
              </Link>
            ))}
          </List>
        )}
      </main>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps<CountriesProps> =
  async () => {
    const response = await axios.get(API.countries, {
      headers: { "x-rapidapi-key": process.env.API_KEY as string },
    });

    return { props: { data: response.data.response } };
  };

export default Countries;
