import { NextPage, GetServerSideProps } from "next";
import React from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import { List, Input, Button } from "@chakra-ui/react";
import axios from "axios";
import { API } from "api";
import { League, Country as CountryType, Season } from "types";
import { CountryCard } from "components";
import styles from "./country.module.css";

interface Response {
  response: {
    league: League;
    seasons: Season[];
    country: CountryType;
  }[];
}

interface CountryProps {
  data: Response["response"];
}

const Country: NextPage<CountryProps> = ({ data }) => {
  const router = useRouter();

  const [searchValue, setSearchValue] = React.useState("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;

    setSearchValue(value.toLowerCase());
  };

  const filteredCountries = data.filter(({ league }) =>
    league.name.toLowerCase().startsWith(searchValue)
  );

  return (
    <div>
      <Head>
        <title>The Choosen Country</title>
        <meta name="description" content="The Choosen Country" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <Button
          colorScheme="teal"
          variant="outline"
          className={styles.backButton}
          onClick={() => router.back()}
        >
          Back
        </Button>
        <h1>
          The Choosen Country: <b>{data[0].country.name}</b>
        </h1>
        <Input
          placeholder="Search the specific national league..."
          className={styles.search}
          onChange={handleChange}
        />
        {filteredCountries.length > 0 && (
          <List className={styles.list}>
            {filteredCountries.map((item) => {
              const { league, seasons } = item;

              return (
                <CountryCard
                  key={league.id}
                  league={league}
                  seasons={seasons}
                />
              );
            })}
          </List>
        )}
      </main>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps<CountryProps> = async ({
  params,
}) => {
  if (params) {
    const {
      data: { response },
    } = await axios.get<Response>(API.leagues, {
      headers: { "x-rapidapi-key": process.env.API_KEY as string },
      params: { country: params.country },
    });

    return { props: { data: response } };
  }

  return { props: { data: [] } };
};

export default Country;
