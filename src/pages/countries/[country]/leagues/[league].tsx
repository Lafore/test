import { NextPage, GetServerSideProps } from "next";
import React from "react";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { Button, Table, Thead, Tbody, Tr, Td, Th } from "@chakra-ui/react";
import axios from "axios";
import { API } from "api";
import { StandingsCell } from "types";
import styles from "./league.module.css";

interface Response {
  response: [{ league: ExtendedLeague }];
}

interface ExtendedLeague {
  id: number;
  season: number;
  name: string;
  country: string;
  logo: string;
  flag: string;
  standings: [StandingsCell[]];
}

interface LeaguePageProps {
  data: ExtendedLeague;
}

const LeaguePage: NextPage<LeaguePageProps> = ({ data }) => {
  const router = useRouter();

  return (
    <div>
      <Head>
        <title>The Choosen League:</title>
        <meta name="description" content="The Choosen League" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Button
          colorScheme="teal"
          variant="outline"
          onClick={() => router.back()}
        >
          Back
        </Button>
        <h1>
          The Choosen League: <b>{data.name}</b>
        </h1>
        {data.standings[0].length > 0 && (
          <Table className={styles.table}>
            <Thead>
              <Tr>
                <Th>#</Th>
                <Th>Team</Th>
                <Th isNumeric>Points</Th>
                <Th isNumeric>+/-</Th>
                <Th isNumeric>Info</Th>
                <Th>Form</Th>
                <Th>Actions</Th>
              </Tr>
            </Thead>
            <Tbody>
              {data.standings[0].map((place) => (
                <Tr key={place.team.id}>
                  <Td>{place.rank}</Td>
                  <Td>
                    <Image
                      width={20}
                      height={20}
                      alt={place.team.name}
                      src={place.team.logo}
                    />
                    <span>{place.team.name}</span>
                  </Td>
                  <Td isNumeric>{place.points}</Td>
                  <Td isNumeric>{place.goalsDiff}</Td>
                  <Td isNumeric>{place.description}</Td>
                  <Td isNumeric>{place.form}</Td>
                  <Td>
                    <Link
                      href={`/teams/${place.team.id}?season=${router.query.season}`}
                    >
                      <a>
                        <Button>Go ahead</Button>
                      </a>
                    </Link>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        )}
      </main>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async ({
  params,
  query,
}) => {
  if (params && query) {
    const {
      data: { response },
    } = await axios.get<Response>(API.standings, {
      headers: { "x-rapidapi-key": process.env.API_KEY as string },
      params: { league: params.league, season: query.season },
    });

    return { props: { data: response[0].league } };
  }

  return { props: { data: {} } };
};

export default LeaguePage;
