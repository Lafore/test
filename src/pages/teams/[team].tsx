import { NextPage, GetServerSideProps } from "next";
import axios from "axios";
import { API } from "api";
import { Statistic } from "types";

interface Response {
  response: Statistic[];
}

interface TeamProps {
  data: Statistic[];
}

const Team: NextPage<TeamProps> = ({ data }) => {
  return <></>;
};

export const getServerSideProps: GetServerSideProps = async ({
  params,
  query,
}) => {
  if (params) {
    const {
      data: { response },
    } = await axios.get<Response>(API.players, {
      headers: { "x-rapidapi-key": process.env.API_KEY as string },
      params: { team: params.team, season: query.season },
    });

    return { props: { data: response } };
  }

  return { props: { data: [] } };
};

export default Team;
