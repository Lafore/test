export const API = {
  countries: process.env.API_HOST + "/countries",
  leagues: process.env.API_HOST + "/leagues",
  standings: process.env.API_HOST + "/standings",
  players: process.env.API_HOST + "/players",
};
