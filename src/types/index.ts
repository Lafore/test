export interface Country {
  name: string;
  code: string;
  flag: string;
}

export interface League {
  id: number;
  logo: string;
  name: string;
  type: string;
}

export interface Team {
  id: number;
  name: string;
  logo: string;
}

export interface Season {
  end: string;
  start: string;
  year: number;
  current: boolean;
}

export interface Player {
  id: number;
  age: number;
  firstname: string;
  lastname: string;
  nationality: string;
  photo: string;
}

export interface Statistic {
  cards: {
    yellow: number;
    red: number;
  };
  goals: {
    total: number;
    saves: number | null;
    assists: number | null;
  };
  games: {
    appearences: number;
    captain: boolean;
    lineups: number;
    minutes: number;
    position: string;
  };
  shots: {
    total: number;
  };
  penalty: {
    commited: number | null;
    missed: number | null;
    saved: number | null;
    scored: number | null;
  };
}

export interface StandingsCell {
  rank: number;
  points: number;
  goalsDiff: number;
  group: string;
  form: string;
  status: string;
  update: string;
  description: string;
  team: Team;
}
